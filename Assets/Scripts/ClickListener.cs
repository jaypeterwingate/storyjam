﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;

[System.Serializable]
public class V3Event : UnityEvent<Vector3> { }

[RequireComponent(typeof(Rigidbody))]
public class ClickListener : MonoBehaviour
{
    [SerializeField] private UnityEvent onClick;
    [SerializeField] private UnityEvent onEnter;
    [SerializeField] private UnityEvent onExit;
    [SerializeField] private V3Event onMove;
    public void InvokeMouseDown()
    {
        onClick?.Invoke();
    }

    public void InvokeHoverEvent(bool isHovered)
    {
        var handler = isHovered ? onEnter : onExit;
        handler?.Invoke();
    }
    
    public void InvokeMoveEvent(Vector3 pos)
    {
        onMove?.Invoke(pos);
    }
}
