﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MapStuff
{
    [RequireComponent(typeof(Rigidbody))]
    public class MapIcon : MonoBehaviour
    {
        [SerializeField] GameObject m_physColParent;
        [SerializeField] GameObject m_rendsParent;
        [SerializeField] MapHandler m_mapHandler;
        
        MeshRenderer[] m_renderers;
        private Rigidbody m_rigid;
        
        // Start is called before the first frame update
        void Start()
        {
            m_renderers = m_rendsParent.GetComponentsInChildren<MeshRenderer>();
            m_rigid = GetComponent<Rigidbody>();
        }
        
        void SetGlow(bool setting)
        {
            for (int i = 0; i < m_renderers.Length; i++)
            {
                m_renderers[i].material.SetInt("GlowOn",setting ? 1 : 0);
            }
        }
        void EnablePhysics(bool setting)
        {
            m_physColParent.SetActive(setting);
            m_rigid.useGravity = setting;
            m_rigid.velocity = new Vector3(0, 0, 0);
            m_rigid.angularVelocity = new Vector3(0, 0, 0);
        }

        public void HandleHoverOn()
        {
            SetGlow(true);
        }
        public void HandleHoverOff()
        {
            SetGlow(false);
        }

        public void HandleSelect()
        {
            HandleHoverOff();
            m_mapHandler.IconClicked(this);
            EnablePhysics(false);
        }
        
        public void HandleDeselect()
        {
            EnablePhysics(true);
        }
    }
}