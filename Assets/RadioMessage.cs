﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RadioMessage : StateMachineBehaviour
{
    [SerializeField] private RadioMessageData data;
    private Radio m_radio;
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        m_radio = animator.GetComponent<Radio>();
        m_radio.StartCoroutine(m_radio.PlayAudioClip(data));
    }
    
}
