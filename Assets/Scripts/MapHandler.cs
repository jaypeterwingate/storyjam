﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MapStuff
{
    public class MapHandler : MonoBehaviour
    {

        [SerializeField] private float m_iconHoverHeight;
        [SerializeField] private CameraController m_camCont;
        
        MapIcon m_selectedIcon;
        bool m_mapActive;

        // Start is called before the first frame update
        void Start()
        {
            
        }

        public void IconClicked(MapIcon clickee)
        {
            if (m_selectedIcon != null) return;
            m_selectedIcon = clickee;
            m_camCont.FocusOnMap_MovingIcon();
        }

        public void HandleMouseMove(Vector3 pos)
        {
            if (m_selectedIcon)
            {
                m_selectedIcon.transform.position = pos + new Vector3(0,m_iconHoverHeight,0);
                //m_selectedIcon.transform.rotation = Quaternion.Lerp(m_selectedIcon.transform.rotation, new Quaternion(0,0,0,0), 0.2f);
            }
        }

        public void CamFocusDrawnAway()
        {
            if (m_selectedIcon) m_selectedIcon.HandleDeselect();
            m_selectedIcon = null;
        }
        
        public void HandleMouseGeneralClick()
        {
            if (m_selectedIcon)
            {
                m_selectedIcon.HandleDeselect();
                m_camCont.FocusOnMap();
                m_selectedIcon = null;
            }
        }
    }
}