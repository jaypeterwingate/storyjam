﻿using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;
using MapStuff;
using UnityEngine;
public enum MouseEvents {
    MouseDown,
    MouseUp,
    MouseOver,
    MouseLeave
}
[RequireComponent(typeof(Camera))]
public class CameraController : MonoBehaviour
{
    private ClickListener m_hoveredItem = null;
    private Camera m_camera;
    private Animator m_animator;
    private int m_cameraFocusId = Animator.StringToHash("CameraFocus");
    private LayerMask m_mask = 1 << 10;
    [SerializeField] private MapHandler m_mapHandler;
    
    void Start()
    {
        m_camera = GetComponent<Camera>();
        m_animator = GetComponent<Animator>();
    }
    public void FocusOnMap()
    {
        m_mask = 1 << 8;
        m_animator.SetInteger(m_cameraFocusId, 10);
    }
    public void FocusOnMap_MovingIcon()
    {
        m_mask = 1 << 11;
        m_animator.SetInteger(m_cameraFocusId, 10);
    }

    public void FocusOnRadio()
    {
        m_mask = 1 << 10;
        m_animator.SetInteger(m_cameraFocusId, 0);
        m_mapHandler.CamFocusDrawnAway();
    }

    public void FocusOnNothing()
    {
        m_mask =1 <<10;
        m_animator.SetInteger(m_cameraFocusId, 0);
        m_mapHandler.CamFocusDrawnAway();
    }
    // Update is called once per frame
    void Update()
    {
        if (GenerateCameraRay(out var hit))
                ProcessRayHit(hit);
        
    }

    private void ProcessRayHit(RaycastHit hit)
    {
        ClickListener listener = null;
        var hasListener = hit.rigidbody && hit.rigidbody.TryGetComponent<ClickListener>(out listener);
        if (m_hoveredItem != listener) 
            HandleHover(listener);
        if (!hasListener) return;
        if (Input.GetMouseButtonDown(0))
            listener.InvokeMouseDown();
        listener.InvokeMoveEvent(hit.point);
    }

    private void HandleHover(ClickListener listener)
    {
        if (m_hoveredItem)
            m_hoveredItem.InvokeHoverEvent(false);
        m_hoveredItem = listener;
        if(m_hoveredItem)
            m_hoveredItem.InvokeHoverEvent(true);
    }

    bool GenerateCameraRay(out RaycastHit result)
    {
        var ray = m_camera.ScreenPointToRay(Input.mousePosition);
        return Physics.Raycast(ray, out result, 10.0f,m_mask);
    } 
}
