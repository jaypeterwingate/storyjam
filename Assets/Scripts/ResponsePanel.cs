﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class ResponsePanel : MonoBehaviour
{

    [SerializeField] private Button button;
    [SerializeField] private TMPro.TextMeshProUGUI text;
    public void LoadData(RadioMessageResponse response, UnityAction<int> selectResponse)
    {
        gameObject.SetActive(response != null);
        if(response == null)
            return;
        text.text = response.response;
        button.onClick.AddListener(() => selectResponse(response.index));
    }
}
