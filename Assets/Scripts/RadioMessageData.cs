﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;

[Serializable]
public enum Operators
{
    Equals,
    LessThan,
    GreaterThan
}
[Serializable]
public class Filter
{
    public string parameter;
    public Operators operation;
    public int value;

    public bool IsTrue(Animator animator)
    {
        var param = animator.GetInteger(parameter);
        switch (operation)
        {
            case Operators.Equals:
                return param == value;
            case Operators.GreaterThan:
                return param > value;
            case Operators.LessThan:
                return param < value;
            default:
                return false;
        }
    }
}

[Serializable]
public class RadioMessageResponse
{
    [HideInInspector]
    public int index;
    [SerializeField]
    public string response;
    [SerializeField]
    public List<Filter> filters;

    public RadioMessageResponse(RadioMessageResponse data, int index)
    {
        this.index = index;
        response = data.response;
        filters = data.filters;
    }
    public bool IsAvailable(Animator animator) => filters.All(filter => filter.IsTrue(animator));
}
[Serializable]
public class RadioMessageData
{
    public AudioClip clip;
    public List<RadioMessageResponse> responses;
    [Range(0.0f, 5.0f)]
    public float postMessageDelay;
    [Range(0.0f, 5.0f)]
    public float postReplyDelay;
}