﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
[RequireComponent(typeof(AudioSource))]
[RequireComponent(typeof(Animator))]
public class Radio : MonoBehaviour
{
     
     private Animator m_animator = null;
     private AudioSource m_source = null;
     private bool m_isRadioOn = false;
     private int m_response = -1;
     private readonly int m_openRadioHash = Animator.StringToHash("radioOpen");
     private readonly int m_responseHash = Animator.StringToHash("response");
     private readonly int m_playNextHash = Animator.StringToHash("playNext");

     private readonly int m_responsesVisible = Animator.StringToHash("responsesVisible");
     [SerializeField] private List<ResponsePanel> m_responsePanels = null;
     [SerializeField] private ParticleSystem m_radioEffect = null;
     
     private List<RadioMessageResponse> m_currentResponses = null;
     private void Start()
     {
          m_animator = GetComponent<Animator>();
          m_source = GetComponent<AudioSource>();
     }

     public void OpenRadio() => ToggleRadio(true);
     public void CloseRadio() => ToggleRadio(false);
     
     private void ToggleRadio(bool newSetting)
     {
          if(newSetting == m_isRadioOn)
               return;
          m_isRadioOn = newSetting;
          m_animator.SetBool(m_openRadioHash, m_isRadioOn);
     }

     public IEnumerator PlayAudioClip(RadioMessageData data)
     {
          // Play our audio
          m_animator.ResetTrigger(m_playNextHash);
          m_source.PlayOneShot(data.clip);
          m_radioEffect.Play();
          yield return new WaitForSeconds(data.clip.length + data.postMessageDelay);
          m_radioEffect.Stop();
          
          // if Available options show them
          m_currentResponses = data.responses
               .Select((response,i) => new RadioMessageResponse(response,i))
               .Where(r => r.IsAvailable(m_animator))
               .ToList();
          
          LoadResponses();
          
          if (m_currentResponses.Count > 0)
          {
               yield return GetUserResponse();     
          }

          yield return new WaitForSeconds(data.postReplyDelay);
          m_animator.SetTrigger(m_playNextHash);
     }

     private void LoadResponses()
     {
          m_animator.SetBool(m_responsesVisible, false);
          for (var i = 0; i < m_responsePanels.Count; i++)
          {
               var response = i < m_currentResponses.Count ? m_currentResponses[i] : null;
               m_responsePanels[i].LoadData(response, SelectResponse);
          }
          if(m_currentResponses.Count > 0)
               m_animator.SetBool(m_responsesVisible, true);
     }

     private void SelectResponse(int index) => m_response = index;
     private IEnumerator GetUserResponse()
     {
          m_response = -1;
          yield return new WaitWhile(() => m_response == -1);
          m_animator.SetInteger(m_responseHash, m_response);
          m_animator.SetBool(m_responsesVisible, false);
     }
}
